<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home/post', 'HomeController@addPost');
Route::post('/home', 'HomeController@addComment');
Route::post('/home/subscriptions', 'HomeController@addSubscriptions');
Route::get('/most/commented/news', 'HomeController@commentedNews');

