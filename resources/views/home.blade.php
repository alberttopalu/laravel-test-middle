@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-12">

            {{------------------User info--------------------}}

            <a href="/home"><button type="button" class="btn btn-info btn-lg" >Home</button></a>

            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#userInfo">запрос профайла</button>

                <!-- Modal -->
                <div id="userInfo" class="modal fade" role="dialog" >
                    <div class="modal-dialog" style="width: 1000px; margin: 0 auto">

                        <!-- Modal content-->
                        <div class="modal-content " >
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"> </h4>
                            </div>
                            <div class="modal-body">

                                    <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>created_at</th>
                                        <th>posts</th>
                                        <th>subscribers</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at}}</td>

                                        <td>
                                         @foreach ($posts_count as $user)
                                            {{ $user['posts_count'] }}
                                         @endforeach
                                        </td>

                                        <td>
                                             @foreach ($subscr_count as $user)
                                                {{ $user['subscriptions_count'] }}
                                            @endforeach
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            {{------------------End User info--------------------}}



            {{------------------Create Post--------------------}}
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#createPost">создание новости</button>

            <div id="createPost" class="modal fade" role="dialog"   >
                <div class="modal-dialog" style="width: 1000px; margin: 0 auto">

                    <!-- Modal content-->
                    <div class="modal-content " >
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> </h4>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal" method="post" action="{{url('/home/post')}}">

                               <input type="hidden" name="_token" value="{{ csrf_token() }}">

                               <div class="form-group">
                                      <label class="col-md-4 control-label">Title</label>
                                       <div class="col-md-6">
                                               <input type="text" class="form-control" name="title" min="1" max="120" required>
                                       </div>
                               </div>

                               <div class="form-group">
                                       <label class="col-md-4 control-label">Text</label>
                                       <div class="col-md-6">
                                           <textarea name="text"  class="form-control" cols="40" rows="3" required ></textarea>
                                       </div>
                               </div>

                               <div class="form-group">
                                       <div class="col-md-6 col-md-offset-4">
                                               <button type="submit" class="btn btn-primary">
                                                       Create
                                               </button>
                                       </div>
                               </div>
                            </form>

                         </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            {{------------------End Create Post--------------------}}


            {{------------------View  Post--------------------}}
            {{--{{var_dump($posts)}}--}}

            <div class="col-md-12" style="margin-top: 2%; text-align: center;  height: 100%">
                <h1>Новости</h1>


                @foreach($posts as  $key=> $value)
                     <div class="col-md-12" style="border: 1px solid black; margin-top: 1%; margin-bottom:2%">
                        <h2 class="col-md-12" style="text-align: center;">{{$value['title']}} </h2>
                        <p class="col-md-12" style="text-align: left;">{{$value['description']}}</p>


                        <div class="pull-left" style="border: 1px solid blue; margin: 2%; padding: 1%">
                            <form class="form-horizontal " method="post" action="{{url('/home/subscriptions')}}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="post_id" value="{{$value['id']}}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Subscriptions</label>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">email</label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="email" min="1" max="50" required >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            subscribe
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                         {{----------------------Comment form---------------------------}}
                         <div class="pull-left" style="border: 1px solid #7fff5e; margin: 2%; padding: 1%; display: block">
                             <form class="form-horizontal " method="post" action="{{url('/home')}}">

                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                 <input type="hidden" name="id" value="{{$value['id']}}">
                                 <div class="form-group">
                                     <label class="col-md-4 control-label">Comment</label>

                                 </div>

                                 <div class="form-group">
                                     <label class="col-md-4 control-label">name</label>
                                     <div class="col-md-6">
                                         <input type="text" class="form-control" name="commentName" min="1" max="50" required >
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <label class="col-md-4 control-label">comment</label>
                                     <div class="col-md-6">
                                         <textarea name="commentText"  class="form-control" cols="40" rows="3" required ></textarea>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="col-md-6 col-md-offset-4">
                                         <button type="submit" class="btn btn-primary">
                                             comment
                                         </button>
                                     </div>
                                 </div>
                             </form>
                         </div>
                         {{----------------------End Comment Form---------------------------}}
                    </div>
                <p>POST COMMENT</p>
                    @foreach($value['comment'] as  $key)
                        <p>{{'name --'.$key['name']}}</p>
                        <p>{{'comment --'.$key['comment']}}</p><br>
                    @endforeach

                @endforeach

            </div>
            {{------------------End View  Post--------------------}}



            {{-------------------------most commented news---------------------------}}

            <a href="/most/commented/news"><button type="button" class="btn btn-info btn-lg"  >наиболее комментируемых новостей</button></a>

            {{-------------------------END most commented news---------------------------}}

        </div>

</div>



@endsection

<?php
/*$arr = ['1', '1','2','3','4',];
 var_dump($arr);*/
$arr = array(2, 5, 5, 3 ,2, 1, 5, 7, 1);
$uniq_arr = array();
foreach ($arr AS $item) {
    if (!in_array($item, $uniq_arr)) {
        $uniq_arr[] = $item;
        echo $item . "\n";
    }
}

?>

