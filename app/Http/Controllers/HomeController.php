<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Subscriptions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts_count = User:: withCount('posts')->where('id', Auth::user()->id)->get()->toArray();
        $subscr_count = User:: withCount('subscriptions')->where('id', Auth::user()->id)->get()->toArray();

        $posts = Post:: with('comment')->get()->toArray();


        return view('/home',  compact('user','posts','posts_count','subscr_count'));
    }

    public function addPost(Request $request)
    {
       $post = new Post();
       $post->user_id = Auth::user()->id;
       $post->title = $request->title;
       $post->description = $request->text;

       $post->save();
       return redirect('/home');
    }

    public function addComment(Request $request)
    {
        $comment = new Comment();
        $comment->post_id = $request->id;
        $comment->name = $request->commentName;
        $comment->comment = $request->commentText;

        $comment->save();
        return redirect('/home');
    }

    public function addSubscriptions(Request $request)
    {
        $subs = new Subscriptions();
        $subs->user_id = Auth::user()->id;
        $subs->post_id = $request->post_id;
        $subs->email = $request->email;
        $subs->save();
        return redirect('/home');
    }

    public function commentedNews()
    {
        $user = Auth::user();
        $posts_count = User::withCount('posts')->where('id', Auth::user()->id)->get()->toArray();
        $subscr_count = User::withCount('subscriptions')->where('id', Auth::user()->id)->get()->toArray();

        $posts = Post::with('commentOrderBy')->take(2)->get()->toArray();


        return view('most-comm-news',  compact('user','posts','posts_count','subscr_count'));
    }


}
