<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function commentOrderBy()
    {
        return $this->hasMany(Comment::class)->orderBy('post_id', 'desc');
    }


}
